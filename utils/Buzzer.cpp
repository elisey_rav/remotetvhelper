#include "Buzzer.h"

static void Buzzer_Task(void *param);

Buzzer::Buzzer(GPIO_TypeDef* gpio, uint16_t pin)
	: m_gpio(gpio), m_pin(pin)
{
	assert_param(IS_GPIO_ALL_PERIPH(m_gpio));
	assert_param(IS_GET_GPIO_PIN(m_pin));

	if (gpio == GPIOA)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	else if (gpio == GPIOB)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	else if (gpio == GPIOC)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	else if (gpio == GPIOD)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
	else if (gpio == GPIOE)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
	else if (gpio == GPIOF)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
	else if (gpio == GPIOG)
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);

	GPIO_InitTypeDef sGpio;
	sGpio.GPIO_Mode = GPIO_Mode_Out_PP;
	sGpio.GPIO_Pin = pin;
	sGpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(gpio, &sGpio);

	xTaskCreate(
				Buzzer_Task,
				"Buzzer_Task",
				configMINIMAL_STACK_SIZE,
				this,
				tskIDLE_PRIORITY + 1,
				NULL);

	vSemaphoreCreateBinary(buzzerSemaphore);
}

static void Buzzer_Task(void *param)
{
	Buzzer *ptrObj = static_cast<Buzzer*>(param);
	ptrObj->buzzerTask();
	while(1);
}

void Buzzer::buzzerTask()
{
	while(1)
	{
		xSemaphoreTake(buzzerSemaphore, portMAX_DELAY);

		int i;
		for (i = 0; i < 50; ++i) {
			GPIO_WriteBit(m_gpio, m_pin, Bit_SET);
			vTaskDelay(1 / portTICK_RATE_MS);
			GPIO_WriteBit(m_gpio, m_pin, Bit_RESET);
			vTaskDelay(1 / portTICK_RATE_MS);
		}
	}
}

void Buzzer::sound()
{
	xSemaphoreGive( buzzerSemaphore );
}

Buzzer buzzer(GPIOC, GPIO_Pin_14);
