#include "ir_receive.h"
#include "ir_receive_queue.h"
#include "UpTimeService.h"
#include "debug.h"
#include "uart.h"
static void IrReceive_Task(void *param);

IrReceive irReceive0;
IrReceive irReceive1;
IrReceive irReceive2;
IrReceive irReceive3;
IrReceive irReceive4;
IrReceive irReceive5;
IrReceive irReceive6;
IrReceive irReceive7;

const unsigned int PREAMBULE_MIN_LENGTH_US = 13000;
const unsigned int PREAMBULE_MAX_LENGTH_US = 14000;

const unsigned int DATA_BIT_ZERO_MIN_LENGTH_US = 920;
const unsigned int DATA_BIT_ZERO_MAX_LENGTH_US = 1320;

const unsigned int DATA_BIT_ONE_MIN_LENGTH_US = 2050;
const unsigned int DATA_BIT_ONE_MAX_LENGTH_US = 2450;

const unsigned int QUEUE_TIMEOUT_FOR_PREAMBULE = 15;
const unsigned int QUEUE_TIMEOUT_FOR_DATA_BIT = 4;

IrReceive::IrReceive()	: rxBitQueue(64)
{
	eventQueue = xQueueCreate( sizeof(uint32_t), 10 );
	xTaskCreate(
				IrReceive_Task,
				"irReceiver",
				configMINIMAL_STACK_SIZE,
				this,
				tskIDLE_PRIORITY + 2,
				NULL);

	waitTime = portMAX_DELAY;
	prevTime = 0;
	parseState = state_wait_begin_preambule;
}

void IrReceive::onExternalIrq()
{
	static bool state = false;
	if (state == false)	{
		state = true;
		pin0_on;
	}
	else	{
		state = false;
		pin0_off;
	}


	uint32_t upTime;
	upTime = upTimeService.getUpTimeUs();

	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xQueueSendFromISR(eventQueue, &upTime, &xHigherPriorityTaskWoken);
	if( xHigherPriorityTaskWoken )	{
		taskYIELD ();
	}
}

static void IrReceive_Task(void *param)
{
	IrReceive *ptrObj = static_cast<IrReceive*>(param);
	ptrObj->parseTask();
	while(1);
}

void IrReceive::parseTask()
{
	while(1)
	{
		uint32_t currentTime;
		bool isEventReceived;

		if (xQueueReceive(eventQueue, &currentTime, waitTime) == pdPASS)	{
			isEventReceived = true;
		}
		else	{
			isEventReceived = false;
		}

		pin3_on;
		uint32_t timeDelta = getTimeDelta(currentTime, prevTime);
		prevTime = currentTime;

		switch (parseState)
		{
		case state_wait_begin_preambule:
			handleWaitBeginPreambuleState();
			break;

		case state_wait_end_preambule:
			handleWaitEndPreambuleState(isEventReceived, timeDelta);
			break;

		case state_parse_bits:
			handleParseBitsState(isEventReceived, timeDelta);
			break;
		}
		pin3_off;
	}
}

void IrReceive::handleWaitBeginPreambuleState()
{
	irReceiveStatistic.beginPreambule();

	parseState = state_wait_end_preambule;
	waitTime = QUEUE_TIMEOUT_FOR_PREAMBULE;
}

void IrReceive::handleWaitEndPreambuleState(bool isEventReceived, uint32_t timeDelta)
{

	if (isEventReceived == false)	{
		irReceiveStatistic.preambuleNoEndDetected();

		parseState = state_wait_begin_preambule;
		waitTime = portMAX_DELAY;
	}
	else if ( (timeDelta < PREAMBULE_MIN_LENGTH_US) || (timeDelta > PREAMBULE_MAX_LENGTH_US))	{
		irReceiveStatistic.preambuleWrongTiming();

		parseState = state_wait_end_preambule;
		waitTime = QUEUE_TIMEOUT_FOR_PREAMBULE;
	}
	else	{
		irReceiveStatistic.endPreambule();

		parseState = state_parse_bits;
		waitTime = QUEUE_TIMEOUT_FOR_DATA_BIT;
		rxBitQueue.clear();
	}
}

void IrReceive::handleParseBitsState(bool isEventReceived, uint32_t timeDelta)
{
	if (isEventReceived == false)	{
		irReceiveStatistic.noMoreDataBit();

		pin0_on;
		parseState = state_wait_begin_preambule;
		waitTime = portMAX_DELAY;
		pin0_off;

		//принять данные
		applyData();

	}
	else if ( (timeDelta < DATA_BIT_ZERO_MIN_LENGTH_US) || (timeDelta > DATA_BIT_ZERO_MAX_LENGTH_US))	{
		pin2_on;
		parseState = state_parse_bits;
		waitTime = QUEUE_TIMEOUT_FOR_DATA_BIT;
		rxBitQueue.insertBit(0);
		pin2_off;
	}
	else if ( (timeDelta < DATA_BIT_ONE_MIN_LENGTH_US) || (timeDelta > DATA_BIT_ONE_MAX_LENGTH_US))	{
		pin2_on;
		parseState = state_parse_bits;
		waitTime = QUEUE_TIMEOUT_FOR_DATA_BIT;
		rxBitQueue.insertBit(1);
		pin2_off;
	}

	else	{
		irReceiveStatistic.dataBitWrongTiming();

		pin1_on;
		parseState = state_wait_end_preambule;
		waitTime = QUEUE_TIMEOUT_FOR_PREAMBULE;
		pin1_off;
		//принять данные
		applyData();
	}
}

uint32_t IrReceive::getTimeDelta(uint32_t currentTime, uint32_t prevTime)
{
	if (currentTime > prevTime)	{
		return currentTime - prevTime;
	}
	else	{
		return ((0xFFFFFFFF - prevTime) + currentTime);
	}
}

void IrReceive::applyData()
{
	if (rxBitQueue.getNumOfBits() == 32)	{

		int i;
		uint32_t receivedData = 0;
		for (i = 0; i < 32; ++i) {
			if (rxBitQueue[i] == true)	{
				SET_BIT(receivedData, 1 << i);
			}
			else	{
				CLEAR_BIT(receivedData, 1 << i);
			}
		}


		irReceiveQueue.putNewItem(receivedData);
		irReceiveStatistic.receivedPacket();

	}
	else {
		irReceiveStatistic.wrongPacketLength();

		UART_SendString("Gg\r\n");
	}

}
