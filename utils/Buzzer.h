#pragma once
#include "stm32f10x.h"

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

class Buzzer {
public:
	Buzzer(GPIO_TypeDef* gpio, uint16_t pin);
	void buzzerTask();
	void sound();

private:
	SemaphoreHandle_t buzzerSemaphore;

	GPIO_TypeDef* m_gpio;
	uint16_t m_pin;
};

extern Buzzer buzzer;
