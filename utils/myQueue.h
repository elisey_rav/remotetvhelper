#pragma once
#include <stdint.h>
#include <string.h>
#include "debug.h"
#include "FreeRTOS.h"
template <typename T>
class myQueue {
public:
	myQueue(unsigned int _maxNumOfItems)
		:	maxNumOfItems(_maxNumOfItems), numOfItems(0)
	{
		ptrItems = (T*)pvPortMalloc( maxNumOfItems * sizeof(T) );
		assert(ptrItems != NULL);
	}

	void push(T &item)
	{
		assert(numOfItems < maxNumOfItems);	//TODO проверить
		memcpy( &ptrItems[numOfItems], &item, sizeof(T) );
		numOfItems++;
	}

/*	bool pop(T* ptrItem)
	{
		if (numOfItems == 0)	{
			return false;
		}
		numOfItems--;
		memcpy(ptrItem, &ptrItems[numOfItems], sizeof(T) );

	}*/

	T operator[](int index)
	{
		assert((unsigned int)(index) < maxNumOfItems);
		return (ptrItems[index]);
	}

	unsigned int getNumOfItems()
	{
		return numOfItems;
	}

	void clear()
	{
		numOfItems = 0;
	}

private:

	T* ptrItems;
	unsigned int maxNumOfItems;
	unsigned int numOfItems;
};

