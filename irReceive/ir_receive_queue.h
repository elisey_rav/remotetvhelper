#pragma once

#include <stdint.h>

#include "UniqueItemHandler.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

typedef enum	{
	appleRemoteCommand_None,
	appleRemoteCommand_Right,
	appleRemoteCommand_Left,
	appleRemoteCommand_Up,
	appleRemoteCommand_Down,
	appleRemoteCommand_Menu,
	appleRemoteCommand_Play
}appleRemoteCommand_t;

class IrReceiveQueue {
public:
	IrReceiveQueue();
	void putNewItem(uint32_t data);
	uint32_t getItem();

	appleRemoteCommand_t getReceivedCommand(uint32_t timeout);

private:
	QueueHandle_t rxIrPacketQueue;

};

extern IrReceiveQueue irReceiveQueue;
