#pragma once
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"

#include "BitQueue.h"
#include "Buzzer.h"
#include "ir_receive_statistic.h"

typedef enum	{
	state_wait_begin_preambule = 1,
	state_wait_end_preambule = 2,
	state_parse_bits = 3
} IrReceive_State_t;

class IrReceive
{
public:
	IrReceive();

	void onExternalIrq();
	void parseTask();

private:
	void handleWaitBeginPreambuleState();
	void handleWaitEndPreambuleState(bool isEventReceived, uint32_t timeDelta);
	void handleParseBitsState(bool isEventReceived, uint32_t timeDelta);
	uint32_t getTimeDelta(uint32_t currentTime, uint32_t prevTime);
	void applyData();

	uint32_t waitTime;
	uint32_t prevTime;
	IrReceive_State_t parseState;

	BitQueue rxBitQueue;

	QueueHandle_t eventQueue;

	IrReceiveStatistic irReceiveStatistic;
};

extern IrReceive irReceive0;
extern IrReceive irReceive1;
extern IrReceive irReceive2;
extern IrReceive irReceive3;
extern IrReceive irReceive4;
extern IrReceive irReceive5;
extern IrReceive irReceive6;
extern IrReceive irReceive7;
