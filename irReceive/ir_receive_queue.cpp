#include "ir_receive_queue.h"
#include "UpTimeService.h"
#include "debug.h"

IrReceiveQueue::IrReceiveQueue()
{
	rxIrPacketQueue = xQueueCreate(5, sizeof(uint32_t));
	assert(rxIrPacketQueue != NULL);
}

void IrReceiveQueue::putNewItem(uint32_t data)
{
	BaseType_t result;
	result = xQueueSend(rxIrPacketQueue, &data, 150);
	assert(result == pdPASS);
}

uint32_t IrReceiveQueue::getItem()
{
	uint32_t data;
	BaseType_t result;
	result = xQueueReceive(rxIrPacketQueue, &data, portMAX_DELAY);
	assert(result == pdPASS);
	return data;
}

appleRemoteCommand_t IrReceiveQueue::getReceivedCommand(uint32_t timeout)
{
	uint32_t data;
	BaseType_t result;
	result = xQueueReceive(rxIrPacketQueue, &data, timeout);
	if (result == pdFAIL)	{
		return appleRemoteCommand_None;
	}


	uint8_t comand = 0;
	comand = ((data >> 16) & 0b1111);

	switch(comand)
	{
		case 0b1000:
			return appleRemoteCommand_Right;
			//UART_SendString("Right");
			break;
		case 0b0100:
			return appleRemoteCommand_Up;
			//UART_SendString("Up");
			break;
		case 0b0010:
			return appleRemoteCommand_Down;
			//UART_SendString("Down");
			break;
		case 0b0111:
			return appleRemoteCommand_Left;
			//UART_SendString("Left");
			break;
		case 0b1101:
			return appleRemoteCommand_Menu;
			//UART_SendString("Menu");
			break;
		case 0b1011:
			return appleRemoteCommand_Play;
			//UART_SendString("Play");
			break;
	}
	return appleRemoteCommand_None;
}

IrReceiveQueue irReceiveQueue;
