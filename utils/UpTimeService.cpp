#include "UpTimeService.h"
#include "stm32f10x.h"
#include "debug.h"
UpTimeService upTimeService;

UpTimeService::UpTimeService()
	: counter(0)
{
	timerInit();
}

uint32_t UpTimeService::getUpTimeUs()
{
	return (counter + getTimerCounter());
}

uint32_t UpTimeService::getUpTimeMs()
{
	return ((counter + getTimerCounter()) / 1000);
}

void UpTimeService::timerInit()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM3, ENABLE );

	TIM_TimeBaseInitTypeDef timerStruct;
	timerStruct.TIM_ClockDivision = 0;
	timerStruct.TIM_CounterMode = TIM_CounterMode_Up;
	timerStruct.TIM_Period = 65000;
	timerStruct.TIM_Prescaler = (72 - 1);
	TIM_TimeBaseInit( TIM3, &timerStruct );

	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
	NVIC_SetPriority(TIM3_IRQn, 3);
	NVIC_EnableIRQ(TIM3_IRQn);
	DBGMCU->CR |= DBGMCU_CR_DBG_TIM3_STOP;

	TIM_Cmd(TIM3, ENABLE);
}

uint16_t UpTimeService::getTimerCounter()
{
	return TIM3->CNT;
}

extern "C"
{
void TIM3_IRQHandler()
{
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	upTimeService.onTimerUpdate();
}
}

void UpTimeService::onTimerUpdate()
{
	counter += 65000;
}
