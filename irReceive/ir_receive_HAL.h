#pragma once

#include <stdint.h>

#define IrReceive_HAL_ExternalIrqPriority	(14u)

void IrReceive_HAL_Init();

void IrReceive_HAL_ExternalIrqEnable();
void IrReceive_HAL_ExternalIrqDisable();
