#pragma once
#include <stdint.h>

class UpTimeService	{
public:
	UpTimeService();
	uint32_t getUpTimeUs();
	uint32_t getUpTimeMs();

	void onTimerUpdate();

private:

	void timerInit();
	uint16_t getTimerCounter();

	uint32_t counter;

	UpTimeService(const UpTimeService& root);
	UpTimeService& operator=(const UpTimeService&);
};

extern UpTimeService upTimeService;
