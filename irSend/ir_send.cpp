#include "ir_send.h"
#include "stm32f10x.h"

const unsigned int PREAMBULE_MODULATION_LENGTH_10US = 120;
const unsigned int PREAMBULE_SILENCE_LENGTH_10US = 40;

const unsigned int DATA_MODULATION_LENGTH_10US = 20;
const unsigned int DATA_ZERO_SILENCE_LENGTH_10US = 60;
const unsigned int DATA_ONE_SILENCE_LENGTH_10US = 20;

IrSend::IrSend()
	:	irSendJobQueue(50), bucyFlag(false)
{
	pwmTimerInit();
	mainTimerInit();
}

void IrSend::sendCommand(command_t command)
{
	bucyFlag = true;
	irSendJobQueue.clear();

	pushJob(255, modulation_on);


	pushFirstStaticPart();

	pushEvenPart();
	pushSecondStaticPart();
	pushCommandPart(command);

	startTransmit();
}

void IrSend::pushFirstStaticPart()
{
	static uint8_t packetBuffer[10] = {0,0,1,0,0,1,0,1,0,1};

	putJobFromBuffer(packetBuffer, 10);
}

void IrSend::pushEvenPart()
{
	static uint8_t packetBufferEven[4] = {1,1,0,0};
	static uint8_t packetBufferNotEven[4] = {0,0,1,1};

	static bool isEven = false;

	if (isEven == true)	{
		isEven = false;
		putJobFromBuffer(packetBufferEven, 4);
	}
	else	{
		isEven = true;
		putJobFromBuffer(packetBufferNotEven, 4);
	}
}

void IrSend::pushSecondStaticPart()
{
	static uint8_t packetBuffer[18] = {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1};

	putJobFromBuffer(packetBuffer, 18);
}

void IrSend::pushCommandPart(command_t command)
{
	static uint8_t packetCommand_power[14] = 				{0,1,0,1,0,1,1,0,1,0,0,1,0,1};
	static uint8_t packetCommand_inputType[14] = 			{0,1,1,0,1,0,1,0,0,1,0,1,0,1};
	static uint8_t packetCommand_command_up[14] =			{1,0,0,1,1,0,1,0,0,1,0,1,0,1};
	static uint8_t packetCommand_command_down[13] = 		{1,0,0,1,1,0,1,0,0,1,0,1,1};
	static uint8_t packetCommand_command_ok[14] =			{1,0,0,1,1,0,1,0,1,0,0,1,0,1};
	static uint8_t packetCommand_command_volumeUp[14] = 	{0,1,0,1,1,0,0,1,0,1,0,1,0,1};
	static uint8_t packetCommand_command_volumeDown[13] = 	{0,1,0,1,1,0,0,1,0,1,0,1,1};
	static uint8_t packetCommand_command_number2[14] = 		{0,1,0,1,0,1,0,1,0,1,1,0,0,1};
	static uint8_t packetCommand_command_number4[14] = 		{0,1,0,1,0,1,0,1,1,0,0,1,0,1};
	static uint8_t packetCommand_command_number5[13] = 		{0,1,0,1,0,1,0,1,1,0,0,1,1};
	static uint8_t packetCommand_command_number6[14] = 		{0,1,0,1,0,1,0,1,1,0,1,0,0,1};

	switch (command) {
		case command_power:
			putJobFromBuffer(packetCommand_power, 14);
			break;
		case command_inputType:
			putJobFromBuffer(packetCommand_inputType, 14);
			break;
		case command_up:
			putJobFromBuffer(packetCommand_command_up, 14);
			break;
		case command_down:
			putJobFromBuffer(packetCommand_command_down, 13);
			break;
		case command_ok:
			putJobFromBuffer(packetCommand_command_ok, 14);
			break;
		case command_volumeUp:
			putJobFromBuffer(packetCommand_command_volumeUp, 14);
			break;
		case command_volumeDown:
			putJobFromBuffer(packetCommand_command_volumeDown, 13);
			break;
		case command_number2:
			putJobFromBuffer(packetCommand_command_number2, 14);
			break;
		case command_number4:
			putJobFromBuffer(packetCommand_command_number4, 14);
			break;
		case command_number5:
			putJobFromBuffer(packetCommand_command_number5, 13);
			break;
		case command_number6:
			putJobFromBuffer(packetCommand_command_number6, 14);
			break;
		default:
			break;
	}
}

void IrSend::putJobFromBuffer(uint8_t *buffer, unsigned int size)
{
	unsigned int i;
	for (i = 0; i < size; ++i) {
		if (buffer[i] == 0)	{
			pushJob(44, modulation_off);
		}
		else	{
			pushJob(44, modulation_on);
		}
	}
}

bool IrSend::IsBusy()
{
	return bucyFlag;
}

void IrSend::whileBucy()
{
	while(bucyFlag)	{
		vTaskDelay(1);
	}
}

extern "C"
{
void TIM2_IRQHandler(void)
{
	TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	irSend.onTimerUpdate();

}
}

void IrSend::onTimerUpdate()
{
	pin0_on;
	if (numOfTransferedJobs < irSendJobQueue.getNumOfItems())	{
		IrSendJobQueue item = irSendJobQueue[numOfTransferedJobs];
		TIM_SetAutoreload(TIM2, item.time_10us - 1);
		setModulationState(item.modulation_state);
		numOfTransferedJobs++;
	}
	else	{
		pin1_on;
		setModulationState(modulation_off);
		stopTransmit();
		bucyFlag = false;
		pin1_off;
	}
	pin0_off;
}

void IrSend::pushJob(uint8_t time, modulation_state_t state)
{
	IrSendJobQueue jobItem(time, state);
	irSendJobQueue.push(jobItem);
}

void IrSend::pwmTimerInit()
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_OCInitTypeDef TIM_OCInitStruct;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_AFIO, ENABLE );
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM4, ENABLE );

	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_8; //PB8
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;   // Alt Function - Push Pull
	GPIO_Init( GPIOB, &GPIO_InitStructure );

	TIM_TimeBaseStructInit( &TIM_TimeBaseInitStruct );
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_Period = 200;		// примерно 56 килогерц
	TIM_TimeBaseInitStruct.TIM_Prescaler = 10-1;
	TIM_TimeBaseInit( TIM4, &TIM_TimeBaseInitStruct );

	TIM_OCStructInit( &TIM_OCInitStruct );
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_Pulse = 100;
	TIM_OC3Init( TIM4, &TIM_OCInitStruct );
}

void IrSend::mainTimerInit()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM2, ENABLE );

	TIM_TimeBaseInitTypeDef timerStruct;
	timerStruct.TIM_ClockDivision = 0;
	timerStruct.TIM_CounterMode = TIM_CounterMode_Up;
	timerStruct.TIM_Period = 200;
	timerStruct.TIM_Prescaler = 720-1;		//период тика таймера - 10 мкс.
	TIM_TimeBaseInit( TIM2, &timerStruct );

	TIM_ARRPreloadConfig(TIM2, DISABLE);

	TIM_SetCounter(TIM2, 0);

	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	NVIC_SetPriority(TIM2_IRQn, 14);
	NVIC_EnableIRQ(TIM2_IRQn);
}

void IrSend::startTransmit()
{
	pin2_on;
	numOfTransferedJobs = 0;

	/*
	 * нужно мгновенное срабатывание прерывания. Для этого счетчик и регистр сравнения устанавливаются
	 * в одно значение, но не равное нулю. Чтобы в прерывании мгновенно не вызывалось следующее прерывание.
	 */
	TIM_SetAutoreload(TIM2, 10);
	TIM_SetCounter(TIM2, 10);
	TIM_Cmd( TIM2, ENABLE );
	pin2_off;
}

void IrSend::stopTransmit()
{
	TIM_Cmd( TIM2, DISABLE );
}

void IrSend::modulationOn()
{
	TIM_SetCounter(TIM4, 0);
	TIM_Cmd( TIM4, ENABLE );
}

void IrSend::modulationOff()
{
	TIM_Cmd( TIM4, DISABLE );
	TIM_SetCounter(TIM4, 256);
}

void IrSend::setModulationState(modulation_state_t modulationState)
{
	if (modulationState == modulation_on)	{
		modulationOn();
	}
	else	{
		modulationOff();
	}
}

IrSend irSend;
