#pragma once
#include <stdint.h>
#include "stm32f10x.h"
#include "debug.h"
#include "FreeRTOS.h"
class BitQueue
{
public:
	BitQueue(unsigned int _maxNumOfBits)	: maxNumOfBits(_maxNumOfBits), currentNumOfBits(0)
	{
		unsigned int numOfBytes;
		if (maxNumOfBits % 8 != 0)	{
			numOfBytes = maxNumOfBits / 8 + 1;
		}
		else	{
			numOfBytes = maxNumOfBits / 8;
		}
		ptrBuffer = (uint8_t*)pvPortMalloc(numOfBytes);
		assert(ptrBuffer != NULL);
	}

	bool operator[](int index)
	{
		assert((unsigned int)(index) < maxNumOfBits);
		unsigned int byteIndex = index / 8;
		unsigned int bitIndex = index % 8;
		return (READ_BIT( ptrBuffer[byteIndex], 1 << bitIndex ));
	}

	void set(unsigned int index)
	{
		assert(index < maxNumOfBits);
		unsigned int byteIndex = index / 8;
		unsigned int bitIndex = index % 8;
		SET_BIT( ptrBuffer[byteIndex], 1 << bitIndex );
	}

	void reset(unsigned int index)
	{
		assert(index < maxNumOfBits);
		unsigned int byteIndex = index / 8;
		unsigned int bitIndex = index % 8;
		CLEAR_BIT( ptrBuffer[byteIndex], 1 << bitIndex );
	}

	void insertBit(bool bitValue)
	{
		assert(currentNumOfBits < maxNumOfBits);

		if (bitValue == true)	{
			set(currentNumOfBits);
		}
		else {
			reset(currentNumOfBits);
		}
		currentNumOfBits++;
	}

	void insertBit(int bitValue)
	{
		assert(currentNumOfBits < maxNumOfBits);

		if (bitValue != 0)	{
			set(currentNumOfBits);
		}
		else {
			reset(currentNumOfBits);
		}
		currentNumOfBits++;
	}

	unsigned int getNumOfBits()
	{
		return currentNumOfBits;
	}

	void clear()
	{
		currentNumOfBits = 0;
	}

private:
	unsigned int maxNumOfBits;
	unsigned int currentNumOfBits;
	uint8_t *ptrBuffer;
};
