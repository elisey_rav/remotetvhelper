#include "FreeRTOS.h"
#include "task.h"

#include "debug.h"
#include "UpTimeService.h"
#include "ir_receive_HAL.h"
#include "ir_send.h"
#include "ir_receive_queue.h"
#include "Buzzer.h"

#include "uart.h"

#include "Led.h"

Led led(GPIOB, GPIO_Pin_11);

static void volumeUp()
{
	led.tougle();

	irSend.sendCommand(command_volumeUp);
	vTaskDelay(120);

	led.tougle();
}

static void volumeDown()
{
	led.tougle();

	irSend.sendCommand(command_volumeDown);
	vTaskDelay(120);

	led.tougle();
}

static void powerCicle()
{
	led.tougle();

	irSend.sendCommand(command_power);
	vTaskDelay(100);

	led.tougle();
}

static void receiverTask(void *param)
{
	UART_SendString("HELLO");

	static appleRemoteCommand_t prevCommand = appleRemoteCommand_Play;
	static unsigned int pressCounter = 0;

	while(1)
	{
		appleRemoteCommand_t command = irReceiveQueue.getReceivedCommand( (pressCounter == 0) ? portMAX_DELAY : 300 );

		if (command == appleRemoteCommand_None)	{
			if ((prevCommand == appleRemoteCommand_Up))	{
				unsigned int i;
				for (i = 0; i < pressCounter - 1; ++i) {
					volumeUp();
				}
			}
			if ((prevCommand == appleRemoteCommand_Down) )	{
				unsigned int i;
				for (i = 0; i < pressCounter - 1; ++i) {
					volumeDown();
				}
			}
			if ((prevCommand == appleRemoteCommand_Menu) && (pressCounter == 3))	{
				powerCicle();
			}

			pressCounter = 0;
			prevCommand = appleRemoteCommand_None;
			continue;
		}

		if ((command != prevCommand) && (prevCommand != appleRemoteCommand_None))	{
			pressCounter = 0;
		}
		else	{

			pressCounter++;

		}
		prevCommand = command;
	}
}

int main(void)
{
	UART_Init();
	Debug_Init();

	IrReceive_HAL_Init();

	xTaskCreate(
				receiverTask,
				"receiverTask",
				configMINIMAL_STACK_SIZE,
				NULL,
				tskIDLE_PRIORITY + 1,
				NULL);


	vTaskStartScheduler();
	assert(0);
}

extern "C"{
void vApplicationMallocFailedHook(void)
{
	while(1);
}

void vApplicationStackOverflowHook(void)
{
	while(1);
}

}
