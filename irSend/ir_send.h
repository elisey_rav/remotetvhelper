#pragma once

#include <stdint.h>
#include "myQueue.h"

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"

typedef enum	{
	command_power,
	command_inputType,
	command_up,
	command_down,
	command_ok,
	command_volumeUp,
	command_volumeDown,
	command_number2,
	command_number4,
	command_number5,
	command_number6
} command_t;


typedef enum	{
	modulation_off = 0,
	modulation_on = 1
} modulation_state_t;

struct IrSendJobQueue
{
	IrSendJobQueue(int time , modulation_state_t state)
		:	time_10us(time), modulation_state(state)
	{
	}
	IrSendJobQueue()
		:	time_10us(0), modulation_state(modulation_off)
	{
	}

	uint8_t time_10us;
	modulation_state_t modulation_state;
};

class IrSend {
public:
	IrSend();
	void sendCommand(command_t command);


	bool IsBusy();
	void whileBucy();
	void onTimerUpdate();

private:
	void pushFirstStaticPart();
	void pushEvenPart();
	void pushSecondStaticPart();
	void pushCommandPart(command_t command);
	void putJobFromBuffer(uint8_t *buffer, unsigned int size);


	void pushJob(uint8_t time, modulation_state_t state);

	void pwmTimerInit();
	void mainTimerInit();
	void startTransmit();
	void stopTransmit();
	void modulationOn();
	void modulationOff();
	void setModulationState(modulation_state_t modulationState);

	myQueue<IrSendJobQueue> irSendJobQueue;
	uint8_t numOfTransferedJobs;
	volatile bool bucyFlag;
};

extern IrSend irSend;
