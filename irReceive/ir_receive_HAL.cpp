#include "ir_receive_HAL.h"
#include "stm32f10x.h"
#include "debug.h"		//for pin_on, pin_off

void prv_gpioInit();
void prv_externalIrqInit();
void prv_timerInit();

void IrReceive_HAL_Init()
{
	prv_gpioInit();
	prv_externalIrqInit();
}

void IrReceive_HAL_ExternalIrqEnable()
{
	EXTI->IMR |= EXTI_IMR_MR0;
/*	EXTI->IMR |= EXTI_IMR_MR1;
	EXTI->IMR |= EXTI_IMR_MR2;
	EXTI->IMR |= EXTI_IMR_MR3;
	EXTI->IMR |= EXTI_IMR_MR4;
	EXTI->IMR |= EXTI_IMR_MR5;
	EXTI->IMR |= EXTI_IMR_MR6;
	EXTI->IMR |= EXTI_IMR_MR7;*/
}

void IrReceive_HAL_ExternalIrqDisable()
{
	EXTI->IMR &= ~(EXTI_IMR_MR0);
/*	EXTI->IMR &= ~(EXTI_IMR_MR1);
	EXTI->IMR &= ~(EXTI_IMR_MR2);
	EXTI->IMR &= ~(EXTI_IMR_MR3);
	EXTI->IMR &= ~(EXTI_IMR_MR4);
	EXTI->IMR &= ~(EXTI_IMR_MR5);
	EXTI->IMR &= ~(EXTI_IMR_MR6);
	EXTI->IMR &= ~(EXTI_IMR_MR7);*/
}

void prv_gpioInit()
{
	GPIO_InitTypeDef gpio;

	gpio.GPIO_Mode = GPIO_Mode_IPU;
	//gpio.GPIO_Pin = 0b11111111;
	gpio.GPIO_Pin = 0b1;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &gpio);
}

void prv_externalIrqInit()
{
	RCC->APB2ENR |= RCC_APB2ENR_AFIOEN;

	// Мультиплексоры на порт C
	AFIO->EXTICR[0] = 0;
	AFIO->EXTICR[0] |=	AFIO_EXTICR1_EXTI0_PA;
	/*AFIO->EXTICR[0] |=	AFIO_EXTICR1_EXTI0_PA |
						AFIO_EXTICR1_EXTI1_PA |
						AFIO_EXTICR1_EXTI2_PA |
						AFIO_EXTICR1_EXTI3_PA;

	AFIO->EXTICR[1] = 0;
	AFIO->EXTICR[1] |=	AFIO_EXTICR2_EXTI4_PA |
						AFIO_EXTICR2_EXTI5_PA |
						AFIO_EXTICR2_EXTI6_PA |
						AFIO_EXTICR2_EXTI7_PA;
*/
	// Прерывния по падающему фронту
/*	EXTI->FTSR |= 0b11111111;
	EXTI->RTSR &= ~(0b11111111);*/

	EXTI->FTSR |= 0b1;
	EXTI->RTSR &= ~(0b1);

	IrReceive_HAL_ExternalIrqEnable();

	NVIC_SetPriority(EXTI0_IRQn, IrReceive_HAL_ExternalIrqPriority);
/*	NVIC_SetPriority(EXTI1_IRQn, IrReceive_HAL_ExternalIrqPriority);
	NVIC_SetPriority(EXTI2_IRQn, IrReceive_HAL_ExternalIrqPriority);
	NVIC_SetPriority(EXTI3_IRQn, IrReceive_HAL_ExternalIrqPriority);
	NVIC_SetPriority(EXTI4_IRQn, IrReceive_HAL_ExternalIrqPriority);
	NVIC_SetPriority(EXTI9_5_IRQn, IrReceive_HAL_ExternalIrqPriority);*/

	NVIC_EnableIRQ(EXTI0_IRQn);
/*	NVIC_EnableIRQ(EXTI1_IRQn);
	NVIC_EnableIRQ(EXTI2_IRQn);
	NVIC_EnableIRQ(EXTI3_IRQn);
	NVIC_EnableIRQ(EXTI4_IRQn);
	NVIC_EnableIRQ(EXTI9_5_IRQn);*/
}

extern "C"
{
#include "ir_receive.h"
void EXTI0_IRQHandler()
{
	EXTI_ClearITPendingBit(EXTI_Line0);
	irReceive0.onExternalIrq();

}

/*void EXTI1_IRQHandler()
{
	irReceive1.onExternalIrq();
	EXTI_ClearITPendingBit(EXTI_Line1);
}

void EXTI2_IRQHandler()
{
	irReceive2.onExternalIrq();
	EXTI_ClearITPendingBit(EXTI_Line2);
}

void EXTI3_IRQHandler()
{
	irReceive3.onExternalIrq();
	EXTI_ClearITPendingBit(EXTI_Line3);
}

void EXTI4_IRQHandler()
{
	irReceive4.onExternalIrq();
	EXTI_ClearITPendingBit(EXTI_Line4);
}

void EXTI9_5_IRQHandler()
{
	if (EXTI_GetITStatus(EXTI_Line5) == SET)	{
		irReceive5.onExternalIrq();
		EXTI_ClearITPendingBit(EXTI_Line5);
	}
	else if (EXTI_GetITStatus(EXTI_Line6) == SET)	{
		irReceive6.onExternalIrq();
		EXTI_ClearITPendingBit(EXTI_Line6);
	}
	else if (EXTI_GetITStatus(EXTI_Line7) == SET)	{
		irReceive7.onExternalIrq();
		EXTI_ClearITPendingBit(EXTI_Line7);
	}
}*/
}
