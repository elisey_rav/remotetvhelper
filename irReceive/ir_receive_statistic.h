#pragma once
#include <stdint.h>

class IrReceiveStatistic
{
public:
	IrReceiveStatistic()	:
		m_beginPreambule(0),
		m_endPreambule(0),
		m_preambuleWrongTiming(0),
		m_preambuleNoEndDetected(0),
		m_noMoreDataBit(0),
		m_dataBitWrongTiming(0),
		m_wrongCheckPacket(0),
		m_receivedPacket(0)
	{
	}

	void print()
	{
	}

	void beginPreambule()
	{
		m_beginPreambule++;
	}

	void endPreambule()
	{
		m_endPreambule++;
	}

	void preambuleWrongTiming()
	{
		m_preambuleWrongTiming++;
	}
	void preambuleNoEndDetected()
	{
		m_preambuleNoEndDetected++;
	}

	void noMoreDataBit()
	{
		m_noMoreDataBit++;
	}

	void dataBitWrongTiming()
	{
		m_dataBitWrongTiming++;
	}

	void wrongPacketLength()
	{
		m_wrongPacketLength++;
	}

	void wrongCheckPacket()
	{
		m_wrongCheckPacket++;
	}

	void receivedPacket()
	{
		m_receivedPacket++;
	}

private:

	uint32_t m_beginPreambule;

	uint32_t m_endPreambule;
	uint32_t m_preambuleWrongTiming;
	uint32_t m_preambuleNoEndDetected;

	uint32_t m_noMoreDataBit;
	uint32_t m_dataBitWrongTiming;

	uint32_t m_wrongPacketLength;
	uint32_t m_wrongCheckPacket;
	uint32_t m_receivedPacket;
};
